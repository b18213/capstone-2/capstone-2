// DEPENDENCIES
const jwt =require('jsonwebtoken');
const secret = "HotelReservationAPI"

module.exports.createAccessToken = (user) =>{
	console.log(user);

	const data = {
		id: user._id,
		email: user.email,
		isAdmin: user.isAdmin
	}

	console.log(data);

	return jwt.sign(data, secret, {})
}

// TOKEN CHECK
module.exports.verify = (req, res, next) => {

	let token = req.headers.authorization;

	if(typeof token === "undefined"){

		return res.send({auth: "Verification failed due to lack of token"})
	} else {

		token = token.slice(7, token.length)
		console.log(token)

		jwt.verify(token, secret, (err, decodedToken) => {

			if(err){
				return res.send({
					auth: "Failed",
					message: err.message
				})
			} else{

				req.user = decodedToken

				next();
			}	
		})
	}
};

// ADMIN VERIFICATION
module.exports.verifyAdmin = (req, res, next) => {

	if(req.user.isAdmin){
		next();
	}else {
		return res.send({
			auth: "Failed",
			message: "Request Denied: User is not an admin"
		})
	}
};