// DEPENDENCIES
const Room = require('../models/room');

// ADD NEW ROOM

module.exports.addNewRoom = (req, res) => {

	let newRoom = new Room({

		roomType: req.body.roomType,
		roomNumber: req.body.roomNumber,
		description: req.body.description,
		price: req.body.price

	});

	newRoom.save()
	.then(room => res.send(room))
	.catch(err => res.send(err));
};

// RETRIEVE ALL ROOMS

module.exports.getAllRooms = (req, res) =>{

	Room.find({})
	.then(result => res.send(result))
	.catch(err => res.send(err));

};

// RETRIEVE SPECIFIC ROOM

module.exports.getSingleRoom = (req, res) =>{

	Room.findById(req.params.id)
	.then(result => res.send(result))
	.catch(err => res.send(err));
}

// UPDATE ROOM INFORMATION

module.exports.updateRoom = (req, res) =>{

	let updates ={
		roomType: req.body.roomType,
		roomNumber: req.body.roomNumber,
		description: req.body.description,
		price: req.body.price

	}

	Room.findByIdAndUpdate(req.params.id, updates, {new: true})
	.then(updatedRoom => res.send(updatedRoom))
	.catch(err => res.send(err))
};

// ARCHIVE ROOM

module.exports.archiveRoom = (req, res) =>{

	let updates ={

		isActive : false
	}

	Room.findByIdAndUpdate(req.params.id, updates, {new: true})
	.then(updatedRoom => res.send(updatedRoom))
	.catch(err => res.send(err));
};

// DELETE RESERVATION

module.exports.cancelReservation = (req, res) =>{

	let newReservedFor = {
		reservedFor: []
	};

	
	Room.findByIdAndUpdate(req.params.id, newReservedFor, {new:true})
	.then(updatedRoom => res.send(updatedRoom))
	.catch(err => res.send(err));

}


