// DEPENDENCIES
const User = require('../models/user');
const bcrypt = require('bcryptjs');
const auth = require('../auth');
const Room = require('../models/room');

// USER REGISTRATION
module.exports.registerUser = (req, res) =>{

	const hashedPW = bcrypt.hashSync(req.body.password, 10);

	let newUser = new User ({
		fullName: req.body.fullName,
		email: req.body.email,
		password: hashedPW,
		mobileNo: req.body.mobileNo
	});

	newUser.save()
	.then(user => res.send(user))
	.catch(err => res.send(err));

};

// GET ALL USERS
module.exports.getAllUsers = (req, res) =>{

	User.find({})
	.then(result => res.send(result))
	.catch(err => res.send(err));
};

// USER LOGIN(GET BEARER TOKEN)

module.exports.userLogin = (req, res) =>{

	User.findOne({email: req.body.email})
	.then(userInput => {

		if(userInput === null){

			return res.send("User does not exist")

		}else {

			const checkPassword = bcrypt.compareSync(req.body.password, userInput.password)

			console.log(checkPassword);

			if(checkPassword){

				return res.send({accessToken: auth.createAccessToken(userInput)})

			}else{

				return res.send("Password does not match the email, please try again.")

			}
		}
	})

	.catch(err => res.send(err));
}

// PROMOTE USER TO ADMIN

module.exports.promoteToAdmin = (req, res) =>{

	let promote = {
		isAdmin : true
	};

	User.findByIdAndUpdate(req.params.id, promote, {new:true})
	.then(updatedUser => res.send(updatedUser))
	.catch(err => res.send(err));
};

// RESERVATION

module.exports.reserve = async (req, res) => {


	console.log(req.user.id)
	console.log(req.body.roomId)

		if(req.user.isAdmin){
			return res.send("Action Forbidden")
		}

		let isUserUpdated = await User.findById(req.user.id).then(user =>{

			console.log(user);

			let newReservation = {
				roomId: req.body.roomId
			}

			user.reservations.push(newReservation)
			return user.save()
			.then(user => true)
			.catch(err => err.message)
		})

			if(isUserUpdated !== true){
				return res.send({message: isUserUpdated})
			}

			let isRoomUpdated = await Room.findById(req.body.roomId).then(room =>{

				console.log(room)

				let reserver = {
					userId: req.user.id
				}

				room.reservedFor.push(reserver)

				return room.save()
				.then(room => true)
				.catch(err => err.message) 
			})

			if(isRoomUpdated !== true){

				return res.send({message: isRoomUpdated})
			}

			if(isUserUpdated && isRoomUpdated){
				return res.send({message: 'Reservation successful!'})
			}

};

// GET RESERVATIONS

module.exports.getReservations = (req, res) =>{

	User.findById(req.user.id)
	.then(result => res.send(result.reservations))
	.catch(err => res.send(err));
};

// GET ALL RESERVATIONS

module.exports.getAllReservations = (req, res) =>{
	
	User.find({})

	.then(result => {

  		let finalResult = [ ]

  		for (let item of result) {

  			let obj = { 

  				id: item.id,
  				fullName: item.fullName,
  				reservations: item.reservations

 				}

			finalResult.push(obj)
		}
		res.send(finalResult)
	})
	.catch(err => res.send(err));

};

// CANCEL RESERVATION

module.exports.cancelReservation = (req, res) =>{

	let newReservation = {
		reservations: []
	};

	
	User.findByIdAndUpdate(req.params.id, newReservation, {new:true})
	.then(result => res.send(result))
	.catch(err => res.send(err));

}

// Test

module.exports.test = (req, res) =>{

	User.findById(req.params.id)
	.then(result => res.send(result.reservations))
}