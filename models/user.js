const mongoose = require('mongoose');
const userSchema = new mongoose.Schema({

	fullName: {
		type: String,
		required: [true, "Full name cannot be left blank"]
	},

	email: {
		type: String,
		required: [true, "email cannot be left blank"]
	},

	password:{
		type: String,
		required: [true, "Password cannot be left blank"]
	},

	mobileNo: {
		type: String,
		required: [true, "Mobile number cannot be left blank"]
	},

	isAdmin: {
		type: Boolean,
		default: false
	},

	reservations: [
		{
			roomId:{
				type: String,
				required: [true, "Room Id is required"]
			},

			status:{
				type: String,
				default: "reserved"
			},

			reservationDate:{
				type: Date,
				default: new Date()
			}

		}

	]
})

module.exports = mongoose.model("User", userSchema);