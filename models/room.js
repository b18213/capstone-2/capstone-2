const mongoose = require('mongoose');
const roomSchema = new mongoose.Schema({

	roomType:{
		type: String,
		require: [true, "Room type cannot be left blank"]
	},

	roomNumber:{
		type: Number,
		require: [true, "Room number cannot be left blank"]
	},

	description:{
		type: String,
		require: [true, "Room description cannot be left blank"]
	},

	price:{
		type: Number,
		return: [true, "Price cannot be left blank"]
	},

	isActive:{
		type: Boolean,
		default: true
	},

	createdOn: {
		type: Date,
		default: new Date()
	},

	reservedFor: [
		{
			userId:{
				type: String,
				require: [true, "User Id is required"]
			},

			dateReserved:{
				type: Date,
				default: new Date()
			},

			status:{
				type: String,
				default: "reserved"
			}
		}
	]
})

module.exports = mongoose.model("Room", roomSchema);