//DEPENDENCIES
const express = require('express');
const mongoose = require('mongoose');
const cors = require ('cors');

//SERVER
const app = express();
const port = process.env.PORT || 4000;

//DATABASE CONNECTION
mongoose.connect("mongodb+srv://admin:admin@wdc028-course-booking.wxavv.mongodb.net/capstone-project-2?retryWrites=true&w=majority", 
	{
		useNewUrlParser: true,
		useUnifiedTopology: true
	}
);

let db = mongoose.connection;
db.on("error", console.error.bind(console, "connection error"));
db.once("open", () => console.log("Successfully connected to MongoDB"));

//MIDDLEWARES
app.use(express.json());
app.use(express.urlencoded({extended: true}));
app.use(cors());

//GROUP ROUTING
const userRoutes = require('./routes/userRoutes');
app.use('/users', userRoutes);

const roomRoutes = require('./routes/roomRoutes');
app.use('/rooms', roomRoutes);

//PORT LISTENER
app.listen(port, () => console.log(`Server is running at ${port}`));