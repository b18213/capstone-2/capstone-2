// DEPENDENCIES
const express = require('express');
const router = express.Router();

// IMPORTED MODULES
const roomController = require('../controllers/roomController')

const auth = require('../auth');

// DESTRUCTURED OBJECT (AUTH MODEL)
const {verify, verifyAdmin} = auth;

// Routes

router.post('/',verify, verifyAdmin, roomController.addNewRoom);

router.get('/', roomController.getAllRooms);

router.get('/singleRoom/:id', roomController.getSingleRoom);

router.put('/:id', verify, verifyAdmin, roomController.updateRoom);

router.put('/archive/:id', verify, verifyAdmin, roomController.archiveRoom);

router.put('/cancelReservation/:id',verify, verifyAdmin, roomController.cancelReservation);

module.exports = router;