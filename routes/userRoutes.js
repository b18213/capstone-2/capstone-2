// DEPENDENCIES
const express = require('express');
const router = express.Router();

// IMPORTED MODULES
const userController = require('../controllers/userController')

const auth = require('../auth');

// DESTRUCTURED OBJECT (AUTH MODEL)
const {verify, verifyAdmin} = auth;

// ROUTES

router.post('/', userController.registerUser);

router.post('/userLogin', userController.userLogin );

router.put('/promoteUser/:id', verify, verifyAdmin, userController.promoteToAdmin);

router.post('/reserve', verify, userController.reserve);

router.get('/reservations', verify, userController.getReservations);

router.get('/allReservations', verify, verifyAdmin, userController.getAllReservations);

router.get('/getAllUsers', verify, verifyAdmin, userController.getAllUsers);

router.put('/cancelReservation/:id', verify, verifyAdmin, userController.cancelReservation);



module.exports = router;